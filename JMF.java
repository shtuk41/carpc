
import java.awt.FlowLayout;

    import java.awt.event.ActionEvent;

    import java.awt.event.WindowAdapter;

    import java.awt.event.WindowEvent;

    import java.util.Vector;

    

    import javax.media.CaptureDeviceInfo;

    import javax.media.CaptureDeviceManager;

    import javax.media.ControllerAdapter;

    import javax.media.ControllerEvent;

    import javax.media.Format;

    import javax.media.Manager;

    import javax.media.NoDataSourceException;

    import javax.media.Player;

    import javax.media.RealizeCompleteEvent;

    import javax.media.format.AudioFormat;

    import javax.media.format.VideoFormat;

    import javax.media.protocol.DataSource;
    
    import javax.swing.JFrame;

    import javax.swing.JPanel;

    

    public class JMF {
    	
    static JPanel             panel       = new JPanel();

    static JFrame             myFrame     = new JFrame();

    static Player             player      = null; 	
    	
    static Vector<CaptureDeviceInfo> devices;
    static Vector<CaptureDeviceInfo> videoDevices;
    static Vector<VideoFormat> videoDeviceFormat;

    public static void main(String[] argv) throws NoDataSourceException, Exception
    {
    	
    	 panel = new JPanel();

         panel.setLayout(new FlowLayout());      

         //CREATE FRAME.

         myFrame = new JFrame();

         myFrame.setVisible(true);

         myFrame.setSize(300,300);

         myFrame.getContentPane().add(panel);

         myFrame.addWindowListener(

           new WindowAdapter(){

             public void windowClosing(WindowEvent event){  

            if (player != null)
              player.close();

               myFrame.dispose();

             }

           }

         );           	
    	
    	
    	CaptureDeviceInfo cdi;
    	devices = CaptureDeviceManager.getDeviceList(null);
    	videoDeviceFormat = new Vector<VideoFormat>();
    	
    	if (devices != null && devices.size() > 0)
    	{
    		int iDeviceCount = devices.size();
    		
    		videoDevices = new Vector<CaptureDeviceInfo>();
    		
    		Format[] formats;
    		
    		for (int i = 0; i < iDeviceCount; i++)
    		{
    			cdi = devices.elementAt(i);
    			formats = cdi.getFormats();
    			
    			for (int j = 0; j < formats.length; j++)
    			{
    				if (formats[j] instanceof VideoFormat)
    				{
    					videoDevices.addElement(cdi);
    					videoDeviceFormat.addElement((VideoFormat)formats[j]);
    					break;
    				}
    			}
    		}
    	}
    	
    	player = Manager.createPlayer(videoDevices.elementAt(0).getLocator());
    	player.addControllerListener(

                new ControllerAdapter(){

                  public void controllerUpdate(ControllerEvent event){  

                    if (event instanceof RealizeCompleteEvent) {

                      panel.add("Center",player.getVisualComponent());

                      panel.add("South" ,player.getControlPanelComponent());

                      myFrame.validate();

                    }

                  }

                }

            );               
        player.start();  
    }

 

    }
