import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;


public class ConfigHandler 
{
	Properties cPathProperties;
	
	public ConfigHandler()
	{
		cPathProperties = new Properties();
		
		try 
		{
			cPathProperties.load(new FileInputStream("path.properties"));
		}
		catch (IOException ex) 
		{
			ex.printStackTrace();
		}
	}
	
	public String getPathProperties(String s)
	{
		return cPathProperties.getProperty(s);
	}
}

