


import java.awt.*;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;


public class CarPcVideoPage extends CarPcPageCommon implements  CarPcPageInterface
{
	private JFileChooser 			cFileChooser;
	private CarPcPlayer 			cCarPcPlayer;
	private ConfigHandler 			cConfigHandler;
	
	public CarPcVideoPage(String strBackgroundImageFileName, JFrame f, CarPcPlayer p)
	{
		super(strBackgroundImageFileName,f);
		cCarPcPlayer = p;
		cConfigHandler = new ConfigHandler();
		cFileChooser = new JFileChooser();
		cFileChooser.setCurrentDirectory(new File(cConfigHandler.getPathProperties("VIDEO_DIRECTORY")));
	}
	
	public void enterPage()
	{
		cCarPcPlayer.setBounds(62,58,700,450);
		cParentComponent.getContentPane().add(cCarPcPlayer.getMediaPlayerComponent());
		cParentComponent.setVisible(true);
	}
	
	public void exitPage()
	{
		cCarPcPlayer.Stop();
		cParentComponent.getContentPane().remove(cCarPcPlayer.getMediaPlayerComponent());
	}
	
	public CarPcPageCommon.LINKS processMouse(int iX, int iY)
	{
		if (iX > 825 && iX < 973 && iY > 9 && iY < 35)
		{
			//BACK
			
			return CarPcPageCommon.LINKS.PAGE_MAIN;
		}
		else if (iX > 825 && iX < 973 && iY > 60 && iY < 87)
		{
			//PLAY
			int returnVal = cFileChooser.showOpenDialog(cParentComponent);
			
			if (returnVal == javax.swing.JFileChooser.OPEN_DIALOG) {
				java.io.File file = cFileChooser.getSelectedFile( );
				cCarPcPlayer.PlayFile(file.toString());
			}
		}
		else if (iX > 825 && iX < 973 && iY > 111 && iY < 136)
		{
			//PAUSE
		}
		else if (iX > 825 && iX < 973 && iY > 160 && iY < 189)
		{
			//STOP
		}
		else if (iX > 825 && iX < 973 && iY > 210 && iY < 238)
		{
			//NEXT
		}
		else if (iX > 825 && iX < 973 && iY > 260 && iY < 288)
		{
			//PREV
		}
		
		
		
		return CarPcPageCommon.LINKS.PAGE_VIDEO;
	}
	
	public void draw(Graphics g)
	{
		g.drawImage(imgBackground, 	0,  0, 	1024, 600,
			  	0,	0,	imgBackground.getWidth(), imgBackground.getHeight(),
			  	null);		
	}
	
	

}

