import java.awt.*;

import javax.swing.*;


public class CarPcMainPage extends CarPcPageCommon implements CarPcPageInterface
{
	public CarPcMainPage(String strBackgroundImageFileName, JFrame f)
	{
		super(strBackgroundImageFileName,f);
	}
	
	public CarPcMainPage(String strBackgroundImageFileName)
	{
		
	}
	
	
	
	public CarPcPageCommon.LINKS processMouse(int iX, int iY)
	{
		if (iX > 357 && iX < 527 && iY > 100 && iY < 140)
		{
			//MUSIC 
			return CarPcPageCommon.LINKS.PAGE_MUSIC;
			
		}
		else if (iX > 357 && iX < 527 && iY > 179 && iY < 214)
		{
			//MUVIES
			return CarPcPageCommon.LINKS.PAGE_VIDEO;
		}
		else if (iX > 888 && iX < 1000 && iY > 20 && iY < 58)
		{
			//EXIT
			return CarPcPageCommon.LINKS.LINKS_EXIT;
		}
		
		
		return CarPcPageCommon.LINKS.PAGE_MAIN;
	}
	
	public void draw(Graphics g)
	{
		g.drawImage(imgBackground, 	0,  0, 	1024, 600,
			  	0,	0,	imgBackground.getWidth(), imgBackground.getHeight(),
			  	null);
	}
	
	public void enterPage()
	{
		
		
	}
	
	public void exitPage()
	{
		
	}
}


