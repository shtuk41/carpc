import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class CarPc extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private CarPcPageCommon.LINKS eLinks;
	
	private CarPcMainPage 		cCarPcMainPage;
	private CarPcVideoPage		cCarPcVideoPage;
	private CarPcMusicPage 		cCarPcMusicPage;
	private CarPcPlayer 		cCarPcPlayer;
	private	CarPcPageInterface	cCurrentPage;
	
	int 	iX, iY;
	
	public CarPc()
	{
		super("CAR PC");
		
		setLayout(null);
		 
		addMouseListener(new MouseHandler());
		
		cCarPcPlayer = new CarPcPlayer();
		 
		cCarPcMainPage = new CarPcMainPage("Menu_On.png",this);
		cCurrentPage = cCarPcMainPage;
		
		cCarPcVideoPage = new CarPcVideoPage("Video_On.png",this,cCarPcPlayer);
		cCarPcMusicPage = new CarPcMusicPage("Music_On.png",this,cCarPcPlayer);		
	 
		setSize(1024,600);
		setUndecorated(true);
		setVisible(true);
		
		eLinks = CarPcPageCommon.LINKS.PAGE_MAIN;
	}
	
    public void update(Graphics g){
        paint(g);
    }

	public void paint(Graphics g)
	{
		super.paint(g);
		
		switch (eLinks)
		{
			case PAGE_MAIN:
				cCarPcMainPage.draw(g);
				break;
			case PAGE_MUSIC:
				cCarPcMusicPage.draw(g);
				break;
			case PAGE_VIDEO:
				cCarPcVideoPage.draw(g);
				break;
			default:
		}
		
		//graphic debug
		//g.setColor(Color.red);
	//	g.drawString("" + iX + " " + iY,60,100);
		
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		CarPc carPc = new CarPc();
		carPc.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Already there
	}
	
	private class MouseHandler extends MouseAdapter
	{
		public void mouseClicked(MouseEvent event)
		{
			iX = 	event.getX();
			iY =  	event.getY();
			
			CarPcPageCommon.LINKS temp = cCurrentPage.processMouse(event.getX(),event.getY());	
			
			if (eLinks != temp)
			{
				cCurrentPage.exitPage();
				switch(temp)
				{
					case PAGE_VIDEO:
						cCurrentPage = cCarPcVideoPage;
						break;
					case PAGE_MUSIC:
						cCurrentPage = cCarPcMusicPage;
						break;
					case LINKS_EXIT:
						setVisible(false);
						dispose();
						System.exit(0);
						break;
					default:
						cCurrentPage = cCarPcMainPage;
						break;
				
				}
				cCurrentPage.enterPage();
				eLinks = temp;
				repaint();
			}
			
			repaint();
		}
	}
}
