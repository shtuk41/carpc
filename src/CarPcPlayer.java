import uk.co.caprica.vlcj.binding.LibVlc;
import uk.co.caprica.vlcj.component.EmbeddedMediaPlayerComponent;
import uk.co.caprica.vlcj.runtime.RuntimeUtil;

import com.sun.jna.Native;
import com.sun.jna.NativeLibrary;


public class CarPcPlayer {

	
	private final EmbeddedMediaPlayerComponent mediaPlayerComponent;
	
	private ConfigHandler cConfigHandler;
	
	public CarPcPlayer()
	{
		cConfigHandler = new ConfigHandler();
		
		String sVlcPath = cConfigHandler.getPathProperties("VLC");
		
		NativeLibrary.addSearchPath(RuntimeUtil.getLibVlcLibraryName(), sVlcPath);
	    Native.loadLibrary(RuntimeUtil.getLibVlcLibraryName(), LibVlc.class);			
		
		 mediaPlayerComponent = new EmbeddedMediaPlayerComponent();
		 //mediaPlayerComponent.setLayout(null);
		 //mediaPlayerComponent.setBounds(550,80,450,450);
	}
	
	public void setBounds(int iX, int iY, int iWidth, int iHeight)
	{
		mediaPlayerComponent.setBounds(iX,iY,iWidth,iHeight);	
	}
	
	public EmbeddedMediaPlayerComponent getMediaPlayerComponent()
	{
		return mediaPlayerComponent;
	}
	
	public void PlayFile(String sFile)
	{
		mediaPlayerComponent.getMediaPlayer().playMedia(sFile);	
	}
	
	public void Stop()
	{
		mediaPlayerComponent.getMediaPlayer().stop();
	}
}
