import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;


public class CarPcPageCommon 
{
	static public enum LINKS {PAGE_MAIN,PAGE_MUSIC,PAGE_VIDEO, LINKS_EXIT};
	protected BufferedImage imgBackground = null;
	protected String strBackgroundImageFileName;
	protected JFrame cParentComponent;
	
	public CarPcPageCommon()
	{
		
	}
	
	public CarPcPageCommon(String strBackgroundImageFileName, JFrame f)
	{
		cParentComponent = f;
		this.strBackgroundImageFileName = strBackgroundImageFileName;
		try 
		{
			imgBackground = ImageIO.read(new File(strBackgroundImageFileName));
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
}
