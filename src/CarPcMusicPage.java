import java.awt.*;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;


public class CarPcMusicPage extends CarPcPageCommon implements CarPcPageInterface
{
	private JFileChooser 			cFileChooser;
	private CarPcPlayer 			cCarPcPlayer;
	private ConfigHandler 			cConfigHandler;
	
	public CarPcMusicPage(String strBackgroundImageFileName, JFrame f, CarPcPlayer p)
	{
		super(strBackgroundImageFileName,f);
		cCarPcPlayer = p;
		cConfigHandler = new ConfigHandler();
		cFileChooser = new JFileChooser();
		cFileChooser.setCurrentDirectory(new File(cConfigHandler.getPathProperties("MUSIC_DIRECTORY")));
	}
	
	public void enterPage()
	{
		cCarPcPlayer.setBounds(441,95,450,450);
		cParentComponent.getContentPane().add(cCarPcPlayer.getMediaPlayerComponent());
		cParentComponent.setVisible(true);
	}
	
	public void exitPage()
	{
		cCarPcPlayer.Stop();
		cParentComponent.getContentPane().remove(cCarPcPlayer.getMediaPlayerComponent());
	}
	
	public CarPcPageCommon.LINKS processMouse(int iX, int iY)
	{
		if (iX > 11 && iX < 159 && iY > 21 && iY < 48)
		{
			return CarPcPageCommon.LINKS.PAGE_MAIN;
		}
		else if (iX > 152 && iX < 265 && iY > 80 && iY < 167)
		{
			int returnVal = cFileChooser.showOpenDialog(cParentComponent );
			
			if (returnVal == javax.swing.JFileChooser.OPEN_DIALOG) {
				java.io.File file = cFileChooser.getSelectedFile( );
				cCarPcPlayer.PlayFile(file.toString());
			}
			
		}
		
		return CarPcPageCommon.LINKS.PAGE_MUSIC;
	}
	
	public void draw(Graphics g)
	{
		g.drawImage(imgBackground, 	0,  0, 	1024, 600,
			  	0,	0,	imgBackground.getWidth(), imgBackground.getHeight(),
			  	null);		
	}
}

