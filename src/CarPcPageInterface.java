
import java.awt.*;

public interface CarPcPageInterface {

	public void enterPage();
	public void exitPage();
	public CarPcPageCommon.LINKS processMouse(int iX, int iY);
	public void draw(Graphics g);
}
